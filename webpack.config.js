const path = require('path');

module.exports = {
	entry: path.resolve(__dirname, './src/index.ts'),
	module: {
		rules: [
			{
				test: /\.([jt]sx?)$/,
				exclude: /node_modules/,
				use: ['babel-loader']
			}
		]
	},
	resolve: {
		extensions: ['*', '.js', '.jsx', '.ts', '.tsx'],
	},
	output: {
		path: path.resolve(__dirname, './public'),
		filename: 'bundle.js',
	},
	devServer: {
	},
}
