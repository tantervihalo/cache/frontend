import React, {createRef, MouseEventHandler} from "react"
import * as Gitlab from '@gitbeaker/core'

const url = "https://tantervihalo-cache-worker.lezsak.workers.dev/"
const checkInterval = 500
const finalStatuses = "success, failed, canceled, skipped"

type WorkerDataT = {
    files: {
        name?: string
        metadata?: {
            date: string
            headers: [name: string, value: string][]
        }
    }[]
    latest: {
        file?: string
        pipeline?: Gitlab.Types.PipelineExtendedSchema // | {error: string}
    }
}

function File({name}: { name: string }) {
    const match = name.match(/([^\/?]*)(\?.*)$/)
    const text = decodeURI(match?.[1] || name)

    return (
        <a href={(new URL(name, url)).toString()}>{text}</a>
    )
}

export default class App extends React.Component<{}, {
    data: undefined | WorkerDataT
    uploadStatus: 'none' | 'loading' | 'waiting' | 'uploading' | 'failed' | 'succeeded'
    file?: File
    arrayBuffer?: ArrayBuffer
    lastDuration?: number
}> {
    protected updatesEnabled: boolean = false
    private readonly fileRef: React.RefObject<HTMLInputElement>

    constructor(props: {}) {
        super(props)

        this.fileRef = createRef()

        this.state = {
            data: undefined,
            uploadStatus: 'none',
        }
    }

    componentDidMount() {
        this.updatesEnabled = true
        // noinspection JSIgnoredPromiseFromCall
        this.reload()
    }

    componentWillUnmount() {
        this.updatesEnabled = false
    }

    async reload() {
        if (!this.updatesEnabled) return
        const res = await fetch(url, {
            headers: {},
        })
        try {
            const data = await res.json() as WorkerDataT
            await new Promise(resolve => this.setState({data}, () => resolve(undefined)))
            if (this.state.data?.latest.pipeline?.duration) {
                const lastDuration = parseInt(this.state.data.latest.pipeline.duration)
                await new Promise(resolve => this.setState({lastDuration}, () => resolve(undefined)))
            }
        } catch (e) {
            const data = undefined
            await new Promise(resolve => this.setState({data}, () => resolve(undefined)))
        }

        if (!finalStatuses.split(/, /g).includes(this.state.data?.latest.pipeline?.status || "")) {
            await new Promise(resolve => setTimeout(() => resolve(undefined), checkInterval))
            // noinspection ES6MissingAwait
            this.reload()
        }
    }

    get latestFile() {
        const data = this.state.data
        if (!data) return
        const name = data.latest.file
        if (!name) return
        const result = data.files.find(file => file.name === name)
        if (!result) return
        return result
    }

    renderStatus() {
        const data = this.state.data
        if (!data) {
            return "Loading..."
        }

        const rows: JSX.Element[] = [
            <tr key="numberOfFiles">
                <th>Number of files uploaded:</th>
                <td>{data.files.length}</td>
            </tr>,
        ]

        const latestFile = this.latestFile
        if (latestFile) {
            if (latestFile.name) {
                rows.push(
                    <tr key="latestFile">
                        <th>Latest file:</th>
                        <td><File name={latestFile.name}/></td>
                    </tr>,
                )
            }

            if (latestFile.metadata?.date) {
                rows.push(
                    <tr key="date">
                        <th>Date of latest upload:</th>
                        <td>{(new Date(latestFile.metadata.date)).toString()}</td>
                    </tr>,
                )
            }
        }

        const pipeline = data.latest.pipeline
        if (pipeline) {
            if (pipeline.error) {
                rows.push(
                    <tr key="status">
                        <th>Processing status:</th>
                        <td>Error</td>
                    </tr>,
                    <tr key="error">
                        <th>Error:</th>
                        <td>{pipeline.error as string}</td>
                    </tr>,
                )
            } else {
                rows.push(
                    <tr key="status">
                        <th>Processing status:</th>
                        <td>
                            {pipeline.status}
                            <img src={(new URL((pipeline.detailed_status as any).favicon, pipeline.web_url)).toString()} style={{height: '1em'}}/>
                        </td>
                    </tr>,
                    <tr key="link">
                        <th>Pipeline ID:</th>
                        <td>
                            <a href={pipeline.web_url} style={{textDecoration: 'none'}}>
                                <pre>{pipeline.id}</pre>
                            </a>
                        </td>
                    </tr>,
                )

                if (finalStatuses.split(/, /g).includes(pipeline.status)) {
                    rows.push(
                        <tr key="duration">
                            <th>Last processing took:</th>
                            <td>{pipeline.duration} seconds</td>
                        </tr>,
                    )
                } else {
                    // @ts-ignore: date difference
                    const elapsed = (new Date() - new Date(pipeline.created_at)) / 1000

                    rows.push(
                        <tr key="duration">
                            <th>Elapsed time:</th>
                            <td>
                                {Math.round(elapsed)} seconds
                                {this.state.lastDuration && <progress style={{marginLeft: '1em'}} max={this.state.lastDuration} value={elapsed}/>}
                            </td>
                        </tr>,
                    )
                }
            }
        }

        return (
            <table className="dataTable">
                <tbody>
                {rows}
                </tbody>
            </table>
        )
    }

    renderUpdateForm() {
        switch (this.state.uploadStatus) {
            case 'waiting':
                return (
                    <p>
                        <span>Upload new version: </span>
                        <a href="#" onClick={event => {
                            event.preventDefault()
                            const popup = window.open('https://ik-xlsx.sources.gitlab.halo-inf-elte-hu.tk/debug.html', 'preview', 'popup')
                            if (!popup) return
                            window.addEventListener('message', event => {
                                if (event.source === popup) {
                                    if (event.data?.type === 'ready') {
                                        popup.postMessage({
                                            type: 'init',
                                            detail: {
                                                bytes: this.state.arrayBuffer
                                            }
                                        }, '*')
                                    }
                                }
                            })
                        }}>Click here for preview</a>
                        <span> </span>
                        <button onClick={event => {
                            event.preventDefault()
                            this.setState({uploadStatus: 'uploading'})
                            fetch(url + (this.state.file as File).name, {
                                // @ts-ignore
                                mode: 'cors',
                                method: 'put',
                                body: this.state.arrayBuffer,
                            }).then(res => {
                                this.setState({uploadStatus: 'succeeded'})
                                // noinspection ES6MissingAwait,JSIgnoredPromiseFromCall
                                this.reload()
                            }, e => {
                                this.setState({uploadStatus: 'failed'})
                                console.error(e)
                            })
                        }}>✅ Approve</button>
                        <button onClick={event => {
                            event.preventDefault()
                            this.setState({uploadStatus: 'none', file: undefined, arrayBuffer: undefined})
                        }}>❌ Cancel</button>
                    </p>
                )

            case 'uploading':
                return (
                    <p>
                        <span>Upload new version: </span>
                        <progress/>
                        Uploading...
                    </p>
                )

            case 'succeeded':
                return (
                    <p>✅ Uploaded, changes will take effect soon</p>
                )

            case 'failed':
                return (
                    <p>❌ Error, something went wrong</p>
                )

            default:
                return (
                    <p>
                        <span>Update: </span>
                        <input ref={this.fileRef} type="file"
                               disabled={this.state.uploadStatus !== 'none'}
                               onInput={async event => {
                                   // @ts-ignore
                                   const file = this.fileRef.current.files[0]
                                   this.setState({uploadStatus: 'loading', file})
                                   const arrayBuffer: ArrayBuffer = await file.arrayBuffer()
                                   this.setState({uploadStatus: 'waiting', arrayBuffer})
                               }}/>
                    </p>
                )
        }
    }

    renderFileList() {
        if (!this.state.data?.files) return

        const rows: (JSX.Element | undefined)[] = this.state.data.files
            .map(({name, metadata}) => {
                if (!name) return

                const deleteHandler: MouseEventHandler<HTMLButtonElement> = (event) => {
                    event.preventDefault()
                    fetch((new URL(name, url)).toString(), {
                        method: 'delete',
                    }).then(response => {
                        if (!response.ok) {
                            throw Error(response.statusText || `HTTP error ${response.status}`)
                        }
                    }).then(() => this.reload(), e => {
                        console.error(e)
                        alert(e)
                    })
                }

                return (
                    <li key={name}>
                        <div><File name={name as string}/></div>
                        <div className="buttonList">
                            <button className="deleteButton" onClick={deleteHandler}
                                    disabled={name === this.state.data?.latest.file}>
                                🗑️ Delete
                            </button>
                        </div>
                    </li>
                )
            })

        const reloadHandler: MouseEventHandler<HTMLButtonElement> = (event) => {
            event.preventDefault()

            fetch(url, {
                method: 'delete',
            }).then(response => {
                if (!response.ok) {
                    throw Error(response.statusText || `HTTP error ${response.status}`)
                }
            }).then(() => this.reload(), e => {
                console.error(e)
                alert(e)
            })
        }

        return (
            <article className="fileList">
                <button className="bigButton" onClick={reloadHandler}
                        title="⚠️ Performs a full reload. Please try just reloading the page (F5) before using this button.">
                    🔃 Reload
                </button>
                <h3>Files stored:</h3>
                <ul>{rows.filter(v => v) as JSX.Element[]}</ul>
            </article>
        )
    }

    render() {
        return (
            <div>
                <h1>Tantervihalo management</h1>
                {this.renderStatus()}
                <pre style={{overflow: 'hidden'}}>{JSON.stringify(this.state.data)}</pre>
                {this.renderUpdateForm()}
                {this.renderFileList()}
            </div>
        )
    }
}
