import React, {createElement} from 'react'
import {createRoot} from 'react-dom/client'
import App from './App'

document.addEventListener('DOMContentLoaded', () => {
    const root = createRoot(document.body.appendChild(document.createElement('main')))
    root.render(createElement(React.StrictMode, null, createElement(App)))
})
